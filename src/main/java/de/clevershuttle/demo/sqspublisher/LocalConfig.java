package de.clevershuttle.demo.sqspublisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;

@Configuration
@Profile("local")
public class LocalConfig {

	Logger LOGGER= LoggerFactory.getLogger(LocalConfig.class);

	@Bean
	@Primary
	public AmazonSQSAsync localSQSAsync(){
		LOGGER.debug("creating AmazonSQSAsync for localstack");
		return AmazonSQSAsyncClientBuilder
				.standard()
				.withCredentials(
						new AWSStaticCredentialsProvider(
							new BasicAWSCredentials(
									"LocalStackDummyAccessKey",
									"LocalStackDummySecretKey"
							)
						)
				)
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(
								"http://localhost:4576",
								"us-east-1")
				)
				.build();
	}
}
