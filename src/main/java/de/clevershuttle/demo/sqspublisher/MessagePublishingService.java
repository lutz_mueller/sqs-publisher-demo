package de.clevershuttle.demo.sqspublisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class MessagePublishingService {


	private final String destination;

	private final QueueMessagingTemplate messagingTemplate;


	public MessagePublishingService(
			@Value("${queue.name}") String destination,
			@Autowired QueueMessagingTemplate messagingTemplate
	) {
		this.destination = destination;
		this.messagingTemplate = messagingTemplate;
	}

	public void publish(String message) {
		messagingTemplate.send(destination, MessageBuilder.withPayload(message).build());
	}
}
