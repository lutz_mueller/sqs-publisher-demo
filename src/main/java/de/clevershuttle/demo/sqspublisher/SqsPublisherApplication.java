package de.clevershuttle.demo.sqspublisher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;

@SpringBootApplication
public class SqsPublisherApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqsPublisherApplication.class, args);
	}



	@Bean
	public QueueMessagingTemplate queueMessagingTemplate(AmazonSQSAsync asa){
		return new QueueMessagingTemplate(asa);
	}




}
