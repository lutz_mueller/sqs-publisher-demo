package de.clevershuttle.demo.sqspublisher;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

	private final MessagePublishingService messagePublishingService;

	public SendMessageController(
			MessagePublishingService messagePublishingService
	) {
		this.messagePublishingService = messagePublishingService;
	}

	@PostMapping("/message")
	public void sendMessage(@RequestBody String message){
		messagePublishingService.publish(message);
	}

}
