# spring-boot with SQS demo

## usage 

### using localstack 

 * startup localstack
  
 follow instructions on https://github.com/localstack/localstack

 * create local sqs queue on localstack
   
 `awslocal sqs create-queue --queue-name my-queue.fifo --attributes FiFoQueue=true`

 * build and run with local profile 

 `mvn spring-boot:run -Dspring-boot.run.profiles=local`

### using live aws


 

## resources
 * https://github.com/localstack/localstack
 * https://aws.amazon.com/de/blogs/developer/using-amazon-sqs-with-spring-boot-and-spring-jms/

